---
title: '1.11'

url: '/java-granxas-ferro/1.11/'
aliases: [
    '/gz/eqt-j-granxa-iron-versions/1.11/',
    '/eqt-j-granxa-iron-versions/1.11/',

    '/gz/java-granxas-ferro/1.11/',
    '/gz/java-granxa-ferro/1.11/',
    '/java-granxas-ferro/1.11/',
    '/java-granxa-ferro/1.11/',

    '/gz/java-granxas-iron/1.11/',
    '/gz/java-granxa-iron/1.11/',
    '/java-granxas-iron/1.11/',
    '/java-granxa-iron/1.11/',

    '/gz/java-farms-ferro/1.11/',
    '/gz/java-farm-ferro/1.11/',
    '/java-farms-ferro/1.11/',
    '/java-farm-ferro/1.11/',

    '/gz/java-farms-iron/1.11/',
    '/gz/java-farm-iron/1.11/',
#    '/java-farms-iron/1.11/',
#    '/java-farm-iron/1.11/',

    '/gz/java-farms-ferro/1.11/',
    '/gz/java-farm-ferro/1.11/',
    '/java-farms-ferro/1.11/',
    '/java-farm-ferro/1.11/',
]

weight: 11100
type: 'bookcase'
bookcase_cover_src: 'fotos_content/versions/1.11.png'
bookcase_cover_src_dark: 'fotos_content/versions/1.11.png'

---
