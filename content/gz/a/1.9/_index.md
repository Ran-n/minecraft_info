---
title: '1.9'

url: '/java-granxas-ferro/1.9/'
aliases: [
    '/gz/eqt-j-granxa-iron-versions/1.9/',
    '/eqt-j-granxa-iron-versions/1.9/',

    '/gz/java-granxas-ferro/1.9/',
    '/gz/java-granxa-ferro/1.9/',
    '/java-granxas-ferro/1.9/',
    '/java-granxa-ferro/1.9/',

    '/gz/java-granxas-iron/1.9/',
    '/gz/java-granxa-iron/1.9/',
    '/java-granxas-iron/1.9/',
    '/java-granxa-iron/1.9/',

    '/gz/java-farms-ferro/1.9/',
    '/gz/java-farm-ferro/1.9/',
    '/java-farms-ferro/1.9/',
    '/java-farm-ferro/1.9/',

    '/gz/java-farms-iron/1.9/',
    '/gz/java-farm-iron/1.9/',
#    '/java-farm-iron/1.9/',
#    '/java-farms-iron/1.9/',

    '/gz/java-farms-ferro/1.9/',
    '/gz/java-farm-ferro/1.9/',
    '/java-farms-ferro/1.9/',
    '/java-farm-ferro/1.9/',
]

weight: 1900
type: 'bookcase'
bookcase_cover_src: 'fotos_content/versions/1.9.png'
bookcase_cover_src_dark: 'fotos_content/versions/1.9.png'

---
