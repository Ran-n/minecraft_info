---
title: '1.13'

url: '/java-granxas-ferro/1.13/'
aliases: [
    '/gz/eqt-j-granxa-iron-versions/1.13/',
    '/eqt-j-granxa-iron-versions/1.13/',

    '/gz/java-granxas-ferro/1.13/',
    '/gz/java-granxa-ferro/1.13/',
    '/java-granxas-ferro/1.13/',
    '/java-granxa-ferro/1.13/',

    '/gz/java-granxas-iron/1.13/',
    '/gz/java-granxa-iron/1.13/',
    '/java-granxas-iron/1.13/',
    '/java-granxa-iron/1.13/',

    '/gz/java-farms-ferro/1.13/',
    '/gz/java-farm-ferro/1.13/',
    '/java-farms-ferro/1.13/',
    '/java-farm-ferro/1.13/',

    '/gz/java-farms-iron/1.13/',
    '/gz/java-farm-iron/1.13/',
#    '/java-farms-iron/1.13/',
#    '/java-farm-iron/1.13/',

    '/gz/java-farms-ferro/1.13/',
    '/gz/java-farm-ferro/1.13/',
    '/java-farms-ferro/1.13/',
    '/java-farm-ferro/1.13/',
]

weight: 11300
type: 'bookcase'
bookcase_cover_src: 'fotos_content/versions/1.13.png'
bookcase_cover_src_dark: 'fotos_content/versions/1.13.png'

---
