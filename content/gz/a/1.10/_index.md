---
title: '1.10'

url: '/java-granxas-ferro/1.10/'
aliases: [
    '/gz/eqt-j-granxa-iron-versions/1.10/',
    '/eqt-j-granxa-iron-versions/1.10/',

    '/gz/java-granxas-ferro/1.10/',
    '/gz/java-granxa-ferro/1.10/',
    '/java-granxas-ferro/1.10/',
    '/java-granxa-ferro/1.10/',

    '/gz/java-granxas-iron/1.10/',
    '/gz/java-granxa-iron/1.10/',
    '/java-granxas-iron/1.10/',
    '/java-granxa-iron/1.10/',

    '/gz/java-farms-ferro/1.10/',
    '/gz/java-farm-ferro/1.10/',
    '/java-farms-ferro/1.10/',
    '/java-farm-ferro/1.10/',

    '/gz/java-farms-iron/1.10/',
    '/gz/java-farm-iron/1.10/',
#    '/java-farms-iron/1.10/',
#    '/java-farm-iron/1.10/',

    '/gz/java-farms-ferro/1.10/',
    '/gz/java-farm-ferro/1.10/',
    '/java-farms-ferro/1.10/',
    '/java-farm-ferro/1.10/',
]

weight: 11000
type: 'bookcase'
bookcase_cover_src: 'fotos_content/versions/1.10.png'
bookcase_cover_src_dark: 'fotos_content/versions/1.10.png'

---
