---
title: '1.12'

url: '/java-granxas-ferro/1.12/'
aliases: [
    '/gz/eqt-j-granxa-iron-versions/1.12/',
    '/eqt-j-granxa-iron-versions/1.12/',

    '/gz/java-granxas-ferro/1.12/',
    '/gz/java-granxa-ferro/1.12/',
    '/java-granxas-ferro/1.12/',
    '/java-granxa-ferro/1.12/',

    '/gz/java-granxas-iron/1.12/',
    '/gz/java-granxa-iron/1.12/',
    '/java-granxas-iron/1.12/',
    '/java-granxa-iron/1.12/',

    '/gz/java-farms-ferro/1.12/',
    '/gz/java-farm-ferro/1.12/',
    '/java-farms-ferro/1.12/',
    '/java-farm-ferro/1.12/',

    '/gz/java-farms-iron/1.12/',
    '/gz/java-farm-iron/1.12/',
#    '/java-farms-iron/1.12/',
#    '/java-farm-iron/1.12/',

    '/gz/java-farms-ferro/1.12/',
    '/gz/java-farm-ferro/1.12/',
    '/java-farms-ferro/1.12/',
    '/java-farm-ferro/1.12/',
]

weight: 11200
type: 'bookcase'
bookcase_cover_src: 'fotos_content/versions/1.12.png'
bookcase_cover_src_dark: 'fotos_content/versions/1.12.png'

---
