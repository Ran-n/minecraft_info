---
title: '1.8'

url: '/java-granxas-ferro/1.8/'
aliases: [
    '/gz/eqt-j-granxa-iron-versions/1.8/',
    '/eqt-j-granxa-iron-versions/1.8/',

    '/gz/java-granxas-ferro/1.8/',
    '/gz/java-granxa-ferro/1.8/',
    '/java-granxas-ferro/1.8/',
    '/java-granxa-ferro/1.8/',

    '/gz/java-granxas-iron/1.8/',
    '/gz/java-granxa-iron/1.8/',
    '/java-granxas-iron/1.8/',
    '/java-granxa-iron/1.8/',

    '/gz/java-farms-ferro/1.8/',
    '/gz/java-farm-ferro/1.8/',
    '/java-farms-ferro/1.8/',
    '/java-farm-ferro/1.8/',

    '/gz/java-farms-iron/1.8/',
    '/gz/java-farm-iron/1.8/',
#    '/java-farms-iron/1.8/',
#    '/java-farm-iron/1.8/',

    '/gz/java-farms-ferro/1.8/',
    '/gz/java-farm-ferro/1.8/',
    '/java-farms-ferro/1.8/',
    '/java-farm-ferro/1.8/',
]

weight: 1800
type: 'bookcase'
bookcase_cover_src: 'fotos_content/versions/1.8.png'
bookcase_cover_src_dark: 'fotos_content/versions/1.8.png'

---
